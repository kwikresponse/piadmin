<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sensor extends Model
{
    protected $fillable = ['name', 'serial', 'location_id', 'sensortype_id', 'branch_id'];

    public function type () {
	return $this->belongsTo(Sensortype::class, 'sensortype_id');
    }

    public function location () {
	return $this->belongsTo(Location::class, 'location_id');
    }

    public function rules() {
	return $this->hasMany(Rule::class);
    }

    public function branch() {
	return $this->belongsTo(Branch::class, 'branch_id');
    }
}
