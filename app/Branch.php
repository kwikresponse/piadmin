<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $fillable = ['name', 'notes'];

    public function sensors() {
	return $this->hasMany(Sensor::class, 'branch_id');
    }

    public function readings() {
	return $this->hasMany(Reading::class, 'branch_id');
    }
}
