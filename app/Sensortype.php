<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sensortype extends Model
{
    protected $fillable = ['name'];
    //
}
