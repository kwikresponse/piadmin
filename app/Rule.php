<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rule extends Model
{
    protected $fillable = ['name', 'endpoint', 'condition', 'value1', 'value2', 'fortime', 'message'];
}
