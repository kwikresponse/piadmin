<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reading extends Model
{
    protected $fillable = ['sensor_id', 'reading', 'branch_id'];
    protected $dates = ['created_at', 'updated_at'];

    public function sensor() {
	return $this->belongsTo(Sensor::class, 'sensor_id');
    }

    public function branch() {
	return $this->belongsTo(Branch::class, 'branch_id');
    }
}
