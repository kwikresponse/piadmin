<?php 

function postData($data) {

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL,"http://192.168.0.105/logLocation");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "data=" . $data);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $server_output = curl_exec ($ch);
    curl_close ($ch);
}

function makeAction($action) {
    if(isset($action['method'])) {
	switch($action['method']) {
	case 'get':
	    return "<a href='" . $url . "'>" . $action['label'];
	    break;
	}
    }
}

function table($class, $options = []) {
    $class = new $class();
    $columns = Schema::getColumnListing($class->getTable());;

    if(!empty($options['perpage']))
	$perpage = $options['perpage'];
    else
	$perpage = 20;

    if(isset($options['select'])) {
	$class::select($options['select']);
    } else {
	$class::select('*');
    }

    if(isset($options['columns'])) {
	$columns = $options['columns'];
    }

    $data =  $class->paginate($perpage);

    $tableRows = '';
    foreach($data as $row) {
	$tableRows .= '<tr>';
	foreach($columns as $column) {
	    $tableRows .= '<td>' . $row->$column . '</td>';
	}
	if(isset($options['actions'])) {
	    $tableRows .= '<td>';
	    foreach($options['actions'] as $action) {
		
	    }
	    $tableRows .= '</td>';
	}
	$tableRows .= '</tr>';
    }

    $tableHeader = '';
    foreach($columns as $column) {
	$tableHeader .= '<td>' . $column . '</td>';
    }
    if(isset($options['actions'])) {
	$tableHeader .= '<td>Actions</td>';
    }
    $tableHeader = '<th>' . $tableHeader . '</th>';

    return '<table>' . $tableHeader . $tableRows . '</table>';
}
