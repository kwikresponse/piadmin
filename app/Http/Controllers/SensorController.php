<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sensor;

class SensorController extends Controller
{
    public function index() {
	return view('sensors.index', ['sensors' => Sensor::paginate(20)]);
    }


    public function show(Sensor $sensor, Request $request) {
    }

    public function store(Request $request) {
	sensor::create($request->all());
	return redirect()->route('sensors.create');
    }

    public function create() {
	$devices = [];
	if(file_exists('/sys/bus/w1/devices/'))
	    $devices = scandir('/sys/bus/w1/devices/');

	$devices = array_filter($devices, function ($item) {
	    return !in_array($item, ['.', '..', 'w1_bus_master1']);
	});

	return view('sensors.create', ['devices' => $devices]);
    }

    public function update(sensor $sensor, Request $request) {
	$sensor->fill($request->all());
	$sensor->save();
	return redirect()->route('sensors.edit', ['sensor' => $sensor]);
    }

    public function edit(sensor $sensor) {
	$devices = [];
	if(file_exists('/sys/bus/w1/devices/'))
	    $devices = scandir('/sys/bus/w1/devices/');

	$devices = array_filter($devices, function ($item) {
	    return !in_array($item, ['.', '..', 'w1_bus_master1']);
	});

	return view('sensors.edit', ['sensor' => $sensor, 'devices' => $devices]);
    }

    public function destroy(sensor $sensor) {
	$sensor->delete();
	return redirect()->route('sensors.index');
    }
}
