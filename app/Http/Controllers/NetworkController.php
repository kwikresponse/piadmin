<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Network;

class NetworkController extends Controller
{
    public function index() {
	return view('networks.index', ['networks' => Network::paginate(20)]);
    }


    public function show(network $network) {
    }

    public function store(Request $request) {
	network::create($request->all());
	return redirect()->route('networks.create');
    }

    public function getNetworks() {
	$ssids = shell_exec('iwlist wlan1 scan | grep ESSID');
	$ssids = explode("\n", $ssids);
	$networks = [];
	foreach($ssids as $ssid) {
	    $network = substr(str_replace('ESSID:"', '', trim($ssid)), 0, -1);
	    if($network) {
		$networks[] = $network;
	    }
	}
	return response()->json(['networks' => $networks]);
    }

    public function publishNetworks() {
	$firstLine = "ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev \nupdate_config=1";
	$networks = Network::all();
	$data = $firstLine . "\n";

	foreach($networks as $network) {
	    $data .= shell_exec('wpa_passphrase "' . $network->ssid . '"  "' . $network->psk . '"');
	}

	file_put_contents('/etc/wpa_supplicant/wpa_supplicant.conf', $data);
	echo "done <br><a href='" . route('networks.index') . "'> networks </a>";
    }

    public function create() {
	return view('networks.create');
    }

    public function update(network $network, Request $request) {
	$network->fill($request->all());
	$network->save();
	return redirect()->route('networks.edit', ['network' => $network]);
    }

    public function edit(network $network) {
	return view('networks.edit', ['network' => $network]);
    }

    public function destroy(network $network) {
	$network->delete();
	return redirect()->route('networks.index');
    }
    //
}
