<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Location;

class LocationController extends Controller
{
    public function index() {
	return view('locations.index', ['locations' => Location::paginate(20)]);
    }


    public function show(Location $location) {
    }

    public function store(Request $request) {
	Location::create($request->all());
	return redirect()->route('locations.create');
    }

    public function create() {
	return view('locations.create');
    }

    public function update(Location $location, Request $request) {
	$location->fill($request->all());
	$location->save();
	return redirect()->route('locations.edit', ['location' => $location]);
    }

    public function edit(Location $location) {
	return view('locations.edit', ['location' => $location]);
    }

    public function destroy(Location $location) {
	$location->delete();
	return redirect()->route('locations.index');
    }
}
