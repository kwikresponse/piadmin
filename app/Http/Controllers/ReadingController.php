<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reading;
use App\Branch;

class ReadingController extends Controller
{
    public function index(Branch $branch = null) {
	if($branch) {
	    $readings = $branch->readings()->orderBy('id', 'desc')->paginate(50);
	} else {
	    $readings = Reading::orderBy('id', 'desc')->paginate(50);
	}

	return view('readings.index', ['readings' => $readings, 'branches' => Branch::all()]);
    }
}
