<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sensortype;

class SensortypeController extends Controller
{
    public function index() {
	return view('sensortypes.index', ['sensortypes' => Sensortype::paginate(20)]);
    }


    public function show(Sensortype $sensortype) {
    }

    public function store(Request $request) {
	Sensortype::create($request->all());
	return redirect()->route('sensortypes.create');
    }

    public function create() {
	return view('sensortypes.create');
    }

    public function update(Sensortype $sensortype, Request $request) {
	$sensortype->fill($request->all());
	$sensortype->save();
	return redirect()->route('sensortypes.edit', ['sensortype' => $sensortype]);
    }

    public function edit(Sensortype $sensortype) {
	return view('sensortypes.edit', ['sensortype' => $sensortype]);
    }

    public function destroy(Sensortype $sensortype) {
	$sensortype->delete();
	return redirect()->route('sensortypes.index');
    }
}
