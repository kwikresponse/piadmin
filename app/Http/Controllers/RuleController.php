<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rule;

class RuleController extends Controller
{
    public function index() {
	return view('rules.index', ['rules' => Rule::paginate(20)]);
    }


    public function show(rule $rule) {
    }

    public function store(Request $request) {
	rule::create($request->all());
	return redirect()->route('rules.create');
    }

    public function create() {
	return view('rules.create');
    }

    public function update(rule $rule, Request $request) {
	$rule->fill($request->all());
	$rule->save();
	return redirect()->route('rules.edit', ['rule' => $rule]);
    }

    public function edit(rule $rule) {
	return view('rules.edit', ['rule' => $rule]);
    }

    public function destroy(rule $rule) {
	$rule->delete();
	return redirect()->route('rules.index');
    }
}
