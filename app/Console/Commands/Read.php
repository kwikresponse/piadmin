<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Sensor;
use App\Reading;
use App\Location;

class Read extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'read:temp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

//	postData(response()->json(Location::all()->toArray()));

	$sensors = Sensor::all();
	$mainDir = '/sys/bus/w1/devices/';
	foreach($sensors as $sensor) {
	    $file = $mainDir . $sensor->serial . '/w1_slave';
	    if(file_exists($file)) {
		$output = file_get_contents($file);
		$reading = substr($output, strpos($output, 't=') + 2);
		$reading = floatval($reading)/1000;
		Reading::create(['sensor_id' => $sensor->id, 'reading' => $reading, 'branch_id' => $sensor->branch_id]);
	    }
	}
    }
}
