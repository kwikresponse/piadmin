@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
		<div class="panel-heading">readings</div>

                <div class="panel-body">
		    @foreach($branches as $branch)
		    <a href="{{ route('branchreadings', ['branch' => $branch->id]) }}"> {{$branch->name}} </a> | 
		    @endforeach
		    <table class="table table-striped jambo_table bulk_action">
			<thead>
			    <tr class="headings">
				<th>sensor</th>
				<th>branch</th>
				<th>reading</th>
				<th>time</th>
			    </tr>
			</thead>

			<tbody>
			    @foreach($readings as $reading)
			    <tr class="even pointer">
				<td>{{ $reading->sensor->name }} at {{ $reading->sensor->location->name }} serial: {{ $reading->sensor->serial }}</td>
				<td>{{ $reading->branch->name }}</td>
				<td>{{ $reading->reading }}</td>
				<td>{{ $reading->created_at->toDateTimeString() }}</td>
			    </tr>
			    @endforeach
			</tbody>
		    </table>
		    {{ $readings->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
