@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
		<div class="panel-heading">Locations <a href="{{ route('locations.create') }}">create <i class="fa fa-plus"></i></a></div>

                <div class="panel-body">
		    <table class="table table-striped jambo_table bulk_action">
			<thead>
			    <tr class="headings">
				<th>name</th>
				<th>notes</th>
				<th>actions</th>
			    </tr>
			</thead>

			<tbody>
			    @foreach($locations as $location)
			    <tr class="even pointer">
				<td>{{ $location->name }}</td>
				<td>{{ $location->notes }}</td>
				<td>
				    <a href="{{ route('locations.edit', ['location' => $location]) }}" class="btn btn-success">edit</a>
				    {!! Form::model($location, ['method' => 'DELETE', 'route' => ['locations.destroy', $location->id], 
				    'onsubmit' => 'return confirm("sure you want to delete")']) !!}
				    {!! Form::submit('DELETE', ['class' => 'btn btn-danger']) !!}
				    {!! Form::close() !!}
				</td>
			    </tr>
			    @endforeach
			</tbody>
		    </table>
		    {{ $locations->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
