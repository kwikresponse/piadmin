@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
		<div class="panel-heading">Sensors <a href="{{ route('sensors.create') }}">create <i class="fa fa-plus"></i></a></div>

                <div class="panel-body">
		    <table class="table table-striped jambo_table bulk_action">
			<thead>
			    <tr class="headings">
				<th>name</th>
				<th>serial</th>
				<th>location</th>
				<th>type</th>
				<th>actions</th>
			    </tr>
			</thead>

			<tbody>
			    @foreach($sensors as $sensor)
			    <tr class="even pointer">
				<td>{{ $sensor->name }}</td>
				<td>{{ $sensor->serial }}</td>
				<td>{{ $sensor->location->name }} at branch: {{ $sensor->branch->name }}</td>
				<td>{{ $sensor->type->name }}</td>
				<td>
				    <a href="{{ route('sensors.edit', ['sensor' => $sensor]) }}" class="btn btn-success">edit</a>
				    {!! Form::model($sensor, ['method' => 'DELETE', 'route' => ['sensors.destroy', $sensor->id], 
				    'onsubmit' => 'return confirm("sure you want to delete")']) !!}
				    {!! Form::submit('DELETE', ['class' => 'btn btn-danger']) !!}
				    {!! Form::close() !!}
				</td>
			    </tr>
			    @endforeach
			</tbody>
		    </table>
		    {{ $sensors->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
