@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit sensor</div>

                <div class="panel-body">
		    {!! Form::model($sensor, ['method' => 'PUT', 'route' => ['sensors.update', $sensor->id]]) !!}
			@include('partials.create-edit-sensors', ['sensor' => $sensor])
		    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
