@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit sensor type</div>

                <div class="panel-body">
		    {!! Form::model($sensortype, ['method' => 'PUT', 'route' => ['sensortypes.update', $sensortype->id]]) !!}
			@include('partials.create-edit-sensortypes', ['sensortype' => $sensortype])
		    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
