@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Create new sensor type</div>

                <div class="panel-body">
		    {!! Form::open(['method' => 'POST', 'route' => 'sensortypes.store']) !!}
			@include('partials.create-edit-sensortypes')
		    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
