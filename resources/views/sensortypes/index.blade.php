@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
		<div class="panel-heading">Sensor Types <a href="{{ route('sensortypes.create') }}">create <i class="fa fa-plus"></i></a></div>

                <div class="panel-body">
		    <table class="table table-striped jambo_table bulk_action">
			<thead>
			    <tr class="headings">
				<th>name</th>
				<th>actions</th>
			    </tr>
			</thead>

			<tbody>
			    @foreach($sensortypes as $sensortype)
			    <tr class="even pointer">
				<td>{{ $sensortype->name }}</td>
				<td>
				    <a href="{{ route('sensortypes.edit', ['sensortype' => $sensortype]) }}" class="btn btn-success">edit</a>
				    {!! Form::model($sensortype, ['method' => 'DELETE', 'route' => ['sensortypes.destroy', $sensortype->id], 
				    'onsubmit' => 'return confirm("sure you want to delete")']) !!}
				    {!! Form::submit('DELETE', ['class' => 'btn btn-danger']) !!}
				    {!! Form::close() !!}
				</td>
			    </tr>
			    @endforeach
			</tbody>
		    </table>
		    {{ $sensortypes->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
