<div class="form-group required">
    {!! Form::label('name', 'Name') !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name']) !!}
    @if ($errors->has('name'))
    <span class="bar-warning">
	<strong style="color: red">{{ $errors->first('name') }}</strong>
    </span>
    @endif
</div>

<div class="form-group required">
    {!! Form::label('endpoint', 'End point') !!}
    {!! Form::text('endpoint', null, ['class' => 'form-control', 'placeholder' => 'End point']) !!}
    @if ($errors->has('endpoint'))
    <span class="bar-warning">
	<strong style="color: red">{{ $errors->first('endpoint') }}</strong>
    </span>
    @endif
</div>

<div class="form-group required">
    {!! Form::label('condition', 'Reading is ') !!}
    {!! Form::select('condition', ['>', '>=', '<', '<=', 'between'], ['class' => 'form-control', 'placeholder' => 'Condition']) !!}
    @if ($errors->has('condition'))
    <span class="bar-warning">
	<strong style="color: red">{{ $errors->first('condition') }}</strong>
    </span>
    @endif
</div>

<div class="form-group required">
    {!! Form::label('value1', 'Value 1') !!}
    {!! Form::text('value1', null, ['class' => 'form-control', 'placeholder' => 'End point']) !!}
    @if ($errors->has('value1'))
    <span class="bar-warning">
	<strong style="color: red">{{ $errors->first('value1') }}</strong>
    </span>
    @endif
</div>

<div class="form-group required">
    {!! Form::label('value2', 'Value 2') !!}
    {!! Form::text('value2', null, ['class' => 'form-control', 'placeholder' => 'End point']) !!}
    @if ($errors->has('value2'))
    <span class="bar-warning">
	<strong style="color: red">{{ $errors->first('value2') }}</strong>
    </span>
    @endif
</div>

<div class="form-group required">
    {!! Form::label('fortime', 'For time in minutes') !!}
    {!! Form::number('fortime', null, ['class' => 'form-control', 'placeholder' => 'End point']) !!}
    @if ($errors->has('fortime'))
    <span class="bar-warning">
	<strong style="color: red">{{ $errors->first('fortime') }}</strong>
    </span>
    @endif
</div>

<div class="form-group required">
    {!! Form::label('message', 'Message') !!}
    {!! Form::textarea('message', null, ['class' => 'form-control', 'placeholder' => 'End point']) !!}
    @if ($errors->has('message'))
    <span class="bar-warning">
	<strong style="color: red">{{ $errors->first('message') }}</strong>
    </span>
    @endif
</div>

<div class="form-group pull-right">
    {!! Form::submit('save', ['class' => 'form-control']) !!}
</div>
