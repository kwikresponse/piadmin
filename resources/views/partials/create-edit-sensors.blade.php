<div class="form-group">
    <label>Connected sensors serials</label>
    <ul id="devices">
	@foreach($devices as $device)
	<li style="cursor: pointer">{{$device}}</li>
	@endforeach
    </ul>
</div>

<script>
$(document).ready(function () {
    $('#devices li').click(function () {
	$('#serial').val($(this).html());
    });
});
</script>

<div class="form-group required">
    {!! Form::label('name', 'Name') !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name']) !!}
    @if ($errors->has('name'))
    <span class="bar-warning">
	<strong style="color: red">{{ $errors->first('name') }}</strong>
    </span>
    @endif
</div>

<div class="form-group required">
    {!! Form::label('serial', 'Serial') !!}
    {!! Form::text('serial', null, ['class' => 'form-control', 'placeholder' => 'Serial number']) !!}
    @if ($errors->has('serial'))
    <span class="bar-warning">
	<strong style="color: red">{{ $errors->first('serial') }}</strong>
    </span>
    @endif
</div>

<div class="form-group required">
    {!! Form::label('location_id', 'Location') !!}
    {!! Form::select('location_id', App\Location::all()->pluck('name', 'id'),
	isset($sensor)? $sensor->id: null,
    ['class' => 'form-control']) !!}
    @if ($errors->has('location_id'))
    <span class="bar-warning">
	<strong style="color: red">{{ $errors->first('location_id') }}</strong>
    </span>
    @endif
</div>

<div class="form-group required">
    {!! Form::label('sensortype_id', 'Sensor type') !!}
    {!! Form::select('sensortype_id', App\Sensortype::all()->pluck('name', 'id'),
	isset($sensor)? $sensor->id: null,
    ['class' => 'form-control']) !!}
    @if ($errors->has('sensortype_id'))
    <span class="bar-warning">
	<strong style="color: red">{{ $errors->first('sensortype_id') }}</strong>
    </span>
    @endif
</div>

<div class="form-group required">
    {!! Form::label('branch_id', 'Branch id') !!}
    {!! Form::select('branch_id', App\Branch::all()->pluck('name', 'id'),
	isset($branch)? $branch->id: null,
    ['class' => 'form-control']) !!}
    @if ($errors->has('branch_id'))
    <span class="bar-warning">
	<strong style="color: red">{{ $errors->first('branch_id') }}</strong>
    </span>
    @endif
</div>

<div class="form-group pull-right">
    {!! Form::submit('save', ['class' => 'form-control']) !!}
</div>
