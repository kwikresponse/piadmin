<div class="form-group required">
    {!! Form::label('name', 'Name') !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name']) !!}
    @if ($errors->has('name'))
    <span class="bar-warning">
	<strong style="color: red">{{ $errors->first('name') }}</strong>
    </span>
    @endif
</div>

<div class="form-group pull-right">
    {!! Form::submit('save', ['class' => 'form-control']) !!}
</div>
