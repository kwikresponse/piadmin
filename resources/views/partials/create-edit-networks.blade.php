<div class="form-group">
    <span id="loading">Loading networks...</span>
    <ul id="networks">
    </ul>
</div>

<script>
$(document).ready(function() {
    $.get("{{ route('networks') }}", function (response) {
	$('#loading').remove();
	response.networks.forEach(function (item, index) {
	    $('#networks').append($('<li>').css('cursor', 'pointer').html(item));
	});
    });

    $('#networks').on('click', 'li', function () {
	$('#ssid').val($(this).html());
    });
});
</script>

<div class="form-group required">
    {!! Form::label('ssid', 'SSID') !!}
    {!! Form::text('ssid', null, ['class' => 'form-control', 'placeholder' => 'SSID']) !!}
    @if ($errors->has('ssid'))
    <span class="bar-warning">
	<strong style="color: red">{{ $errors->first('ssid') }}</strong>
    </span>
    @endif
</div>

<div class="form-group required">
    {!! Form::label('psk', 'PSK') !!}
    {!! Form::text('psk', null, ['class' => 'form-control', 'placeholder' => 'PSK']) !!}
    @if ($errors->has('psk'))
    <span class="bar-warning">
	<strong style="color: red">{{ $errors->first('psk') }}</strong>
    </span>
    @endif
</div>

<div class="form-group pull-right">
    {!! Form::submit('save', ['class' => 'form-control']) !!}
</div>
