@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
		<div class="panel-heading">Branches <a href="{{ route('branches.create') }}">create <i class="fa fa-plus"></i></a></div>

                <div class="panel-body">
		    <table class="table table-striped jambo_table bulk_action">
			<thead>
			    <tr class="headings">
				<th>name</th>
				<th>notes</th>
				<th>actions</th>
			    </tr>
			</thead>

			<tbody>
			    @foreach($branches as $branch)
			    <tr class="even pointer">
				<td>{{ $branch->name }}</td>
				<td>{{ $branch->notes }}</td>
				<td>
				    <a href="{{ route('branches.edit', ['branch' => $branch]) }}" class="btn btn-success">edit</a>
				    {!! Form::model($branch, ['method' => 'DELETE', 'route' => ['branches.destroy', $branch->id], 
				    'onsubmit' => 'return confirm("sure you want to delete")']) !!}
				    {!! Form::submit('DELETE', ['class' => 'btn btn-danger']) !!}
				    {!! Form::close() !!}
				</td>
			    </tr>
			    @endforeach
			</tbody>
		    </table>
		    {{ $branches->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
