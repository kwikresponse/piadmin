@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
		    {!! Form::model($network, ['method' => 'PUT', 'route' => ['networks.update', $network->id]]) !!}
			@include('partials.create-edit-networks', ['network' => $network])
		    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
