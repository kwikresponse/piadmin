@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
		<div class="panel-heading">networks 
		    <a href="{{ route('networks.create') }}">create <i class="fa fa-plus"></i></a>
		    <a href="{{ route('networks.publish') }}"> Publish networks </a>
		</div>

                <div class="panel-body">
		    <table class="table table-striped jambo_table bulk_action">
			<thead>
			    <tr class="headings">
				<th>SSID</th>
				<th>PSK</th>
				<th>actions</th>
			    </tr>
			</thead>

			<tbody>
			    @foreach($networks as $network)
			    <tr class="even pointer">
				<td>{{ $network->ssid }}</td>
				<td>{{ $network->psk }}</td>
				<td>
				    <a href="{{ route('networks.edit', ['network' => $network]) }}" class="btn btn-success">edit</a>
				    {!! Form::model($network, ['method' => 'DELETE', 'route' => ['networks.destroy', $network->id], 
				    'onsubmit' => 'return confirm("sure you want to delete")']) !!}
				    {!! Form::submit('DELETE', ['class' => 'btn btn-danger']) !!}
				    {!! Form::close() !!}
				</td>
			    </tr>
			    @endforeach
			</tbody>
		    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
