@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
		<div class="panel-heading">rules <a href="{{ route('rules.create') }}">create <i class="fa fa-plus"></i></a></div>

                <div class="panel-body">
		    <table class="table table-striped jambo_table bulk_action">
			<thead>
			    <tr class="headings">
				<th>name</th>
				<th>sensor</th>
				<th>endpoint</th>
				<th>condition</th>
				<th>value1</th>
				<th>value2</th>
				<th>message</th>
			    </tr>
			</thead>

			<tbody>
			    @foreach($rules as $rule)
			    <tr class="even pointer">
				<td>{{ $rule->name }}</td>
				<td>{{ $rule->sensor_id }}</td>
				<td>{{ $rule->endpoint }}</td>
				<td>{{ $rule->condition }}</td>
				<td>{{ $rule->value1 }}</td>
				<td>{{ $rule->value2 }}</td>
				<td>{{ $rule->message }}</td>
				<td>
				    <a href="{{ route('rules.edit', ['rule' => $rule]) }}" class="btn btn-success">edit</a>
				    {!! Form::model($rule, ['method' => 'DELETE', 'route' => ['rules.destroy', $rule->id], 
				    'onsubmit' => 'return confirm("sure you want to delete")']) !!}
				    {!! Form::submit('DELETE', ['class' => 'btn btn-danger']) !!}
				    {!! Form::close() !!}
				</td>
			    </tr>
			    @endforeach
			</tbody>
		    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
