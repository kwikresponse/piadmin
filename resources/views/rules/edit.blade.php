@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Rule</div>

                <div class="panel-body">
		    {!! Form::model($rule, ['method' => 'PUT', 'route' => ['rules.update', $rule->id]]) !!}
			@include('partials.create-edit-rules', ['rule' => $rule])
		    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
