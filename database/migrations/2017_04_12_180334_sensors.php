<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Sensors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sensors', function (Blueprint $table) {
            $table->increments('id');
	    $table->string('name');
	    $table->string('serial');
	    $table->integer('location_id')->unsigned()->length(10)->nullable();
	    $table->integer('sensortype_id')->unsigned()->length(10)->nullable();
            $table->timestamps();

	    $table->foreign('location_id')->references('id')->on('locations')->onDelete('set null');
	    $table->foreign('sensortype_id')->references('id')->on('sensortypes')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sensors');
    }
}
