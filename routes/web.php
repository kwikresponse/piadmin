<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('locations', 'LocationController');
Route::resource('sensors', 'SensorController');
Route::resource('sensortypes', 'SensortypeController');
Route::resource('rules', 'RuleController');
Route::resource('networks', 'NetworkController');
Route::resource('branches', 'BranchController');

Route::get('/getNetworks', 'NetworkController@getNetworks')->name('networks');
Route::get('/publishNetworks', 'NetworkController@publishNetworks')->name('networks.publish');
Route::get('/readings/{branch}', 'ReadingController@index')->name('branchreadings');
Route::get('/readings', 'ReadingController@index')->name('readings');

Auth::routes();

Route::get('/home', 'HomeController@index');
